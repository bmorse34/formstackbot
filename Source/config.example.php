<?php

use Phergie\Irc\Connection;

return array(
    // Plugins to include for all connections
    'plugins' => array(
        new \Phergie\Irc\Plugin\React\AutoJoin\Plugin(array(
            'channels' => '#channelname',
        )),

        new \bkmorse\Phergie\Irc\Plugin\React\FormStackBot\Plugin(array(
            'prefix' => '!', // string denoting the start of a command
            'pattern' => '/^!/', // PCRE regular expression denoting the presence of a
            'nick' => true, // true to match common ways of addressing the bot by its
                            // connection nick
            'formstack_form_id' => '', // id of form in formstack.com dashboard
            'formstack_token'   => '', // token from when you create an application on formstack.com
        )),
    ),

    'connections' => array(
        new Connection(array(
            // Required settings
            'serverHostname' => 'irc.freenode.net',
            'username' => 'botusername',
            'realname' => 'Bot Real Name',
            'nickname' => 'Bot nickname', // I would set the username and nickname as the same

            // Optional settings

            // 'hostname' => 'user server name goes here if needed',
            // 'serverport' => 6667,
            // 'password' => 'password goes here if needed',
            // 'options' => array(
            //     'transport' => 'ssl',
            //     'force-ipv4' => true,
            // )
        )),
    )
);