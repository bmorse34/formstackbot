#IRC Formstack Bot

##Installation

- Clone repo
- Run composer update in Source directory: composer update
- Create form in formstack dashboard (only supports text type fields at this time)
- Create application in formstack api dashboard
- modify config.example.php and save as config.php in root directory (be sure to include your formstack form id and formstack api token and channel and irc username)
- run phergie in command line, be sure to cd to/path/of/root/project

	./vendor/bin/phergie /path/to/folder/config/is/in/config.php

You will connect to the formstackbot channel, you can go to log in here https://webchat.freenode.net/ (enter a nick/username different than the one you set for your bot in the config file), once you log in, enter /join #channelnam. You will enter the channel, you will see the user for the bot you entered in the chat room. 

You can send them a message (replace botusername with the name you gave your bot in the config file)

	/msg botusername Hi! Whats up?

The bot will ask you if you want to sign up for a newsletter.

##To-do

- allow for more than text type fields to be submitted to
- validate user input